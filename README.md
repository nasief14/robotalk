## WELCOME TO THE ROBOTALK WEB APP
Please sure to read through the entire README.md and understand what steps to take to start your application. It is very important to read through the notes at the end of the document, the notes at the end provide insight to help navigate through the UI. 

## HOW TO RUN ROBOTALK WEB APP

## Initializing Your App
1. Download or Clone Folder from bitbuket repository
2. Open File path where folder has been downloaded eg.C:\Users\Admin\Downloads\robotalk
3. Open up your terminal or cmd and run **npm install**
4. Once the setup is completed withour errors you can move onto the next step

## Starting Server & Connecting To Database
1. Move into the server folder using **cd server**
2. Run the **npm start** command to connect to your database,
if successful you should be presented with a message in the console saying "Connected To The Robotalk Database!"
3. Now your Application is Connected to the Database
4. If you would like to end the connection go into the terminal and hit *ctrl + c* on your keyboard

## Testing Your Connection
1. Navigate to the test folder within the server folder
2. Open the *apiTest.js* file
3. Run the first test to get all wyzebots, you should be presented with a response on the right side of your screen 
4. The response will either show your data or throw a error if there is an issue
5. You are able to run post,get and patch tests within this file
**When Running The Tests Its Best To Do It In Order As You Will Need The *id* From The Response On The Second Test**

## Starting React Front-End
1. While having the connection to the database running open up a new terminal and move into the client\robotalk folder using **cd client\robotalk**
2. Run the **npm run start** command to start up your react enviroment
3. After running the command your terminal should say "Starting the development server"
4. Once completed your brower will open up with the address that gets printed in your terminal
5. Now you are able to Create, Edit and View all of the wyzebots
6. Make sure to read through the notes below before using the app for the first time

## NOTES - VERY IMPORTANT - PLEASE READ BEFORE USING WEB APPLICATION
**Do Not Remove Prewritten Text Inside Image Url On Create Page, Please Type Anything Afterwards**
**When Inserting Your Powers, Please Make Sure To Use A Comma As A Seperator**
**If You Would Like To View More Details About A Individual Wyzebot Feel Free To Click On Either Its Name Or Its Icon**
