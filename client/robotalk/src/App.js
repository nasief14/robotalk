import './App.css';
import Navbar from './components/Navbar';
import Home from './components/Home'
import Create from './components/Create';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import BotDetails from './components/BotDetails';
import Update from './components/Update';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar/>
        <div className="content">
          <Routes>
            <Route path='/' element={<Home/>}/>
            <Route path='/create' element={<Create/>}/>
            <Route path='/update/:id' element={<Update/>}/>
            <Route path='/wyzebot/:id' element={<BotDetails/>}/>
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;
