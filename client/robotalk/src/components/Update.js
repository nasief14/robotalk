import { useParams } from "react-router-dom";
import { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import Axios from 'axios';

const Update = () => {
    const { id } = useParams();
    const [name, setName] = useState("");
    const [image_url, setImage_url] = useState("");
    const [powers, setPowers] = useState("");
    const [squad, setSquad] = useState("");
    const navigate = useNavigate();
    const reload = () => {window.location.reload()};
    console.log("Name :" +name)
    console.log(squad)

    function handleChange(e){
        if(e.target.value === 'Cherry'){
            setSquad("61b712bc1b2adc8f77443e66")
            console.log("Cherry Picked")
            console.log('Squad Is :'+squad+ " Other Sjit " + name)
        } else {
            console.log(e.target.value)
        }
    }



    useEffect(() => {
        Axios.get(`http://localhost:8000/bot/${id}`).then((response) => {
            setName(response.data.data.name);
            setImage_url(response.data.data.image_url);
            setPowers(response.data.data.powers);
            setSquad(response.data.data.squad)
        });
    // eslint-disable-next-line
    },[]);

    const updateWyzebot = () => {
        if(powers.split(',').length >= 4){
            alert("Only A Maximum Of Three Powers Allowed! Please Try Again");
            return;
        }else{
            console.log(name)
            console.log(powers)
            console.log(image_url)
            console.log(squad)
            Axios.patch(`http://localhost:8000/bot/${id}`,{name,powers,image_url,squad})
            .then((response) => {
                console.log(response);
            }).then(
                reload
            ).then(
                navigate('/')
            );
        };
    };

    return (
      <div className="update">
        <h2>Update Wyzebot</h2>
        <form>
          <label>Name:</label>
          <input
            type="text"
            required
            value={name}
            onChange={(e) => setName(e.target.value)}
          />

          <label>Image Url:</label>
          <input
            type="url"
            required
            value={image_url}
            onChange={(e) => setImage_url(e.target.value)}
          />

          <label>Powers:</label>
          <input
            type="text"
            required
            value={powers}
            onChange={(e) => setPowers(e.target.value)}
          />

          <label>Squad:</label>
          <select
            onChange={handleChange}
          >
            <option value="Orange">Orange</option>
            <option value="Radish">Radish</option>
            <option value="Cherry">Cherry</option>
          </select>
          <button onClick={updateWyzebot}>Confirm Update</button>
        </form>
      </div>
    );
};

export default Update;