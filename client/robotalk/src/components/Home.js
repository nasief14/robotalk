import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';

const Home = () => {
    const [Wyzebots, setWyzebots] = useState([]);
    const [isloading, setIsLoading] = useState(true);

    useEffect(() => {
            Axios.get('http://localhost:8000/bots').then((response) => {
                setWyzebots(response.data.data);
                setIsLoading(false);
            });    
    }, []);

    return ( 
        <div className="home">
            { isloading && <div>Content Is Loading...</div>}
            {Wyzebots.map((wyzebot) => {
                return(
                    <div className='wyzebot-cards' key={ wyzebot._id }>
                        <Link to={`/wyzebot/${wyzebot._id}`} style={{textDecoration:'none'}}>
                            <h2>Meet {wyzebot.name}!</h2> 
                            <img className="wyzebot-image" src={wyzebot.image_url} alt="" />
                        </Link>
                    </div>
                ); 
            })}
        </div>
    );
}

export default Home;