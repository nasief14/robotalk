import Axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom"

const Create = () => {
    const [name, setName] = useState("");
    const [image_url, setImage_url] = useState("http://robohash.org/");
    const [powers, setPowers] = useState("");
    const navigate = useNavigate();
    const reload = () => {window.location.reload()}

    const createWyzebot = () => {
        if(powers.split(',').length >= 4){
            alert("Only A Maximum Of Three Powers Allowed! Please Try Again");
            return;
        }else{
            Axios.post('http://localhost:8000/bot',{
                name,
                image_url,
                powers
            }).then((response) => {
                alert('Wyzebot Has Been Created')
            }).then(
                reload,
                navigate('/')
            );
        };
    };

    return (
        <div className="create">
            <h2>Add A New Wyzebot</h2>
            <form onSubmit={createWyzebot}>
                <label>Name:</label>
                <input 
                type='text'
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
                />

                <label>Image Url:</label>
                <input 
                type="url"
                required
                placeholder="Please Use Robohash.org"
                value={image_url}
                onChange={(e) => setImage_url(e.target.value)}
                />

                <label>Powers:</label>
                <input
                type="text"
                required
                placeholder="Maximum Of 3 Powers Allowed - Seperate with a comma"
                value={powers}
                onChange={(e) => setPowers(e.target.value)}
                />

                <button>Create Wyzebot</button>
            </form>
        </div>
    );
}

export default Create;