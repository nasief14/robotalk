import { useParams } from "react-router-dom";
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';

const BotDetails = () => {
    const { id } = useParams();
    const [Wyzebot, setWyzebot] = useState([]);
    const [Squad, setSquad] = useState();
    const [isloading, setIsLoading] = useState(true);

    useEffect(() => {
        Axios.get(`http://localhost:8000/bot/${id}`).then((response) => {
            setWyzebot(response.data.data);
            setSquad(response.data.data.squad.name)
            setIsLoading(false);
        });
    // eslint-disable-next-line
    },[]);

    return(
        <div className="bot-details">
            { isloading && <div>Content Is Loading...</div>}
            <h2>Meet {Wyzebot.name}!</h2>
            <img className="wyzebot-image-details" src={Wyzebot.image_url} alt="" />
            <p>{Wyzebot.name} is a part of the {Squad},<br/>part of the notation Tribe.<br/></p>
            <br/>His powers are:<br/> {Wyzebot.powers}
            <p>This Is a test {Wyzebot.name}</p>
        <div>
        </div>
            <Link to={`/update/${Wyzebot._id}`} style={{textDecoration:'none'}} replace>
            <button>Update Wyzebot</button>
            </Link>
        </div>
    );
};

export default BotDetails;