import { Link } from 'react-router-dom';

const Navbar = () => {
    return ( 
        <nav className="navbar">
            <h1>Home Of The Wyzetalk Robots</h1>
            <div className="links">
                <Link to="/">Home</Link>
                <Link to="/create">New Wyzebot</Link>
            </div>
        </nav>
    );
};

export default Navbar;