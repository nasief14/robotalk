require("dotenv").config();
const express = require('express');
const mongoose = require('mongoose');
const wyzebotController = require('./controllers/wyzebots');
const app = express();
const logger = require('./logger')
const cors = require('cors')

const squadController = require('./controllers/squads')

let PORT = process.env.PORT;
let connectionString = process.env.CONNSTR;

mongoose.connect(connectionString).then(() => {
    app.use(express.json(),logger,cors());

    app.post('/newSquad', squadController.createSquad);
    app.get('/read/:id', squadController.red)
    
    app.patch('/bot/:id', wyzebotController.updateWyzebot);
    app.get('/bot/:id', wyzebotController.readWyzebot);
    app.post('/bot', wyzebotController.createWyzebot);
    app.get('/bots', wyzebotController.readWyzebots);
    
    app.listen(PORT, () => {
        console.log('Connected To Robotalk Database!');
    });
})
.catch((err) => {
    console.log('Connection To Robotalk Database Failed: ' + err);
});