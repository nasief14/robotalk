const Squad = require('../models/Squads');

exports.createSquad = async(req, res) => {
    const squad = req.body;

    try{
        const newSquad = await new Squad({
            name: squad.name,
            wyzebots: squad.wyzebots
        }).save();
        res.send({data:newSquad})
    }catch(err){
        console.log(err)
        res.send({error:"Error Creating Squad"})
    }
};

exports.red = async(req, res) => {
    try{
        const squad = await Squad.findById(req.params.id).populate('wyzebots')
        res.send({data:squad})
    } catch {
        res
        .status(404)
        .send({error:"Unable To Find Squad! - Squad Not Found"})
    }
}