const Wyzebot = require("../models/Wyzebots");

//Create New Wyzebot Functionality
exports.createWyzebot = async (req, res) => {
    const wyzebot = req.body;
    const existingWyzebot = await Wyzebot.findOne({ name: wyzebot.name });
    const powersArray = wyzebot.powers.split(",");
    
    if (existingWyzebot) {
        res
        .status(400)
        .send({ error: "This Wyzebot Name Already Exists!" });
    } else {
        if (powersArray.length <= 0) {
            res
            .status(400)
            .send({ error: "A Minimum Of One Power Is Required!" });
        } else {
            if (powersArray.length >= 4) {
                res
                .status(400)
                .send({ error: "A Maximum Of Three Powers Allowed!" });
            } else {
                try {
                    const newWyzebot = await new Wyzebot({
                        name: wyzebot.name,
                        image_url: wyzebot.image_url,
                        powers: wyzebot.powers,
                        squad: wyzebot.squad
                    }).save();
                    res.send({ data: newWyzebot });
                } catch (err) {
                    console.log(err);
                    res.status(500).send({error:"Unable To Create Wyzebot : " + err});
                };
            };
        };
    };
};

//Read All Wyzebots Functionality
exports.readWyzebots = async (req, res) => {
    const wyzebots = await Wyzebot.find();
    res.send({ data: wyzebots });
};

//Update Individual Wyzebot Functionality
exports.updateWyzebot = async (req, res) => {
    try {
        const bot = await Wyzebot.findById(req.params.id);
        Object.assign(bot, req.body);
        console.log(bot)
        bot.save();
        res.send({ data: bot });
    } catch {
        res
        .status(404)
        .send({ error: "Unable To Update Wyzebot - Wyzebot Not Found!" });
    };
};

//Read Individual Wyzebot Functionality
exports.readWyzebot = async (req, res) => {
    try {
        const wyzebot = await Wyzebot.findById(req.params.id).populate('squad')
        res.send({ data: wyzebot });
    } catch {
        res
        .status(404)
        .send({ error: "Unable To Read Wyzebot - Wyzebot Not Found!" });
    };
};

//Delete Individual Wyzebot Functionality
/*exports.deleteWyzebot = async (req, res) => {
    try {
        const wyzebot = await Wyzebot.findById(req.params.id);
        await wyzebot.remove();
        res.send({ data: true });
    } catch {
        res
        .status(404)
        .send({ error: "Unable To Delete Wyzebot - Wyzebot Not Found!" });
    }
}; */
