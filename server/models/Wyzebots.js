const mongoose = require('mongoose');
const Schema = mongoose.Schema

const WyzebotSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    image_url:{
        type:String,
        required:true
    },
    powers:[
        String        
    ],
    squad:{
        type: Schema.Types.ObjectId,
        ref:'Squads'
    },
    tribe:{
        type:String,
        required:false
    }
});

const WyzebotModel = mongoose.model("Wyzebots", WyzebotSchema);
module.exports = WyzebotModel;