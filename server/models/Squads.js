const mongoose = require('mongoose');
const Schema = mongoose.Schema

const squadSchema = new mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    wyzebots:[{
        type: Schema.Types.ObjectId,
        ref:"Wyzebots"
    }]
});

const squadModel = mongoose.model("Squads", squadSchema);
module.exports = squadModel;