# Full Stack Developer Assignment

## Robotalk

Create your own Wyzebot characters. Each character has a name, image, powers and can belong to a Squad. A Squad is part of a Tribe. 
**Tribe battles are a regular occurence!**

### Meet **Wyzit**! (*sample Wyzebot*)

<img src="https://robohash.org/Wyzit.png?set=set1" height="80px" />

**Wyzit** is a member of the **BigO** squad, part of the **Notation** tribe. His powers are:

- coding
- inquisitiveness
- optimization

----

## What you need to do:

You are building the API to create, update and list new Wyzebot characters:
- Node.js
- Express
  - Logging middleware required
- Typescript
- MongoDB
- Minimum character attributes:
  - name (unique)
  - image url (use robohash.org)
  - powers (min 1, max 3)
- Working tests are required.

You are building a simple frontend that will use the API to manage Wyzebot characters.
- React preferred, but you can use any framework of choice.

### Bonus level one:

You need to extend the API to manage Tribes and Squads:
- A Tribe can have one or many (max 3) Squads.
- A Squad can have one or many (max 5) Wyzebots.
- A Wyzebot can only belong to a single Squad.

You need to extend the frontend that will use the API to manage Tribes, Squads and assign Wyzebots to Squads. 

### Bonus level two:
*Tribe battles are a regular occurence!* You need to extend the API and frontend above to create a new battle between two Tribes and determine a winner. The details are up to you, **be creative**!
- Randomly pick a winning Tribe.
  - Expert level: build predefined rules to determine a winner.
- The details and result of each battle must be recorded in the battle history. 

---

## Notes

Please make sure the basic functionality and requirements are completed (and working) before adding the "icing".  

## How to submit your assignment

Please create a GitHub repository that you can share with Wyzetalk. 

Documentation should be provided in the form of a README file with clear instructions on how to build and run your application.

**Good luck and have fun!**